package com.simsoft.api.samplesevice01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableJpaRepositories("com.simsoft.api.samplesevice01.repository")
public class SampleSevice01Application {

	public static void main(String[] args) {
		SpringApplication.run(SampleSevice01Application.class, args);
	}

}
