package com.simsoft.api.samplesevice01.controller;

import com.simsoft.api.samplesevice01.model.SchoolInfo;
import com.simsoft.api.samplesevice01.repository.SchoolInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/school")
public class SchoolInfoController {

    @Autowired
    private SchoolInfoRepository schoolInfoRepository;

    @GetMapping()
    public ArrayList<SchoolInfo> findAll() {
        return this.schoolInfoRepository.findAll();
    }

    @GetMapping("/{id}")
    public SchoolInfo findById(@PathVariable("id") int id) {
        return this.schoolInfoRepository.findById(id);
    }

    @PostMapping()
    public SchoolInfo save(@RequestBody SchoolInfo schoolInfo) {
        return this.schoolInfoRepository.save(schoolInfo);
    }
}
