package com.simsoft.api.samplesevice02;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("com.simsoft.api.samplesevice02.repository")
public class SampleSevice02Application {

	public static void main(String[] args) {
		SpringApplication.run(SampleSevice02Application.class, args);
	}

}
